package com.eapps.literangesliderexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.eapps.literangeslider.LiteRangeSlider

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<LiteRangeSlider>(R.id.rangeSlider).apply {
            setRange(0, 100)
            setValues(7, 77)
            valueUnit = "%"

            setOnRangeChanged(object : LiteRangeSlider.OnRangeChangedListener {
                override fun onRangeChanged(valueFrom: Int, valueTo: Int) {
                    val toastText = "valueFrom: $valueFrom valueTo: $valueTo"
                    Toast.makeText(applicationContext, toastText, Toast.LENGTH_LONG).show()
                }
            })
        }
    }
}