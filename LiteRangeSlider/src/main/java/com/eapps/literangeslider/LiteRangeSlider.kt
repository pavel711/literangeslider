package com.eapps.literangeslider

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import kotlin.math.abs
import kotlin.math.roundToInt


class LiteRangeSlider(
    context: Context,
    attrs: AttributeSet
) : View(context, attrs) {

    private val paintText = Paint(Paint.ANTI_ALIAS_FLAG)
    private val paintSeekBar = Paint()
    private val rectF = RectF()

    private val thumbRadius = 25F
    private val indicatorMargin = 20F
    private var heightText = 0
    private val progressRadius = 6F
    private val progressCornerRadius = 100F
    private var progressY = 0F

    private var leftIndicatorWidth = 0F
    private var rightIndicatorWidth = 0F
    private var leftTextX = 0F
    private var rightTextX = 0F
    private var textY = 0F

    private var leftThumbX = 0F
    private var rightThumbX = 0F
    private var activeThumb = Thumb.NONE

    private var rangeFrom = 0
    private var rangeTo = 100
    private var thumbStep = 1F
    private var valueFrom = 0
    private var valueTo = 100
    var valueUnit = "₽"

    private var seekBarColorRes = R.color.green
    private var inactiveSeekBarColorRes = R.color.lightGray
    private var textColorRes = R.color.black

    private var callback: OnRangeChangedListener? = null

    init {
        initAttrs(attrs)
    }

    fun setRange(rangeFrom: Int, rangeTo: Int) {
        if (rangeTo >= rangeFrom) {
            this.rangeFrom = rangeFrom
            this.rangeTo = rangeTo
            recalculateThumbStep()
        }
    }

    fun setValues(valueFrom: Int, valueTo: Int) {
        if (valueTo >= valueFrom
            && valueFrom in rangeFrom..rangeTo
            && valueTo in rangeFrom..rangeTo) {
            this.valueFrom = valueFrom
            this.valueTo = valueTo
            invalidate()
        }
    }

    @SuppressLint("Recycle", "CustomViewStyleable", "ResourceAsColor")
    private fun initAttrs(attrs: AttributeSet) {
        val attrsTypedArray = context.obtainStyledAttributes(attrs, R.styleable.LiteRangeSlider)
        seekBarColorRes = attrsTypedArray.getColor(
            R.styleable.LiteRangeSlider_colorSeekBar,
            R.color.green
        )
        inactiveSeekBarColorRes = attrsTypedArray.getColor(
            R.styleable.LiteRangeSlider_colorInactiveSeekBar,
            R.color.lightGray
        )
        textColorRes = attrsTypedArray.getColor(
            R.styleable.LiteRangeSlider_colorText,
            textColorRes
        )
    }

    private fun recalculateThumbStep() {
        thumbStep =  (width - 2 * thumbRadius) / (rangeTo - rangeFrom)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val fontMetrics = paintText.fontMetrics
        var heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        paintText.textSize = (12f * resources.displayMetrics.scaledDensity).roundToInt().toFloat()
        heightText = MeasureSpec.getSize((fontMetrics.bottom - fontMetrics.top).toInt())

        heightSize = when {
            heightMode == MeasureSpec.EXACTLY -> {
                MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY)
            }
            heightMode == MeasureSpec.AT_MOST
                    && heightSize == ViewGroup.LayoutParams.MATCH_PARENT -> {
                MeasureSpec.makeMeasureSpec(
                    (parent as ViewGroup).measuredHeight,
                    MeasureSpec.AT_MOST
                )
            }
            else -> {
                MeasureSpec.makeMeasureSpec(thumbRadius.toInt() * 2 + heightText + indicatorMargin.toInt(), MeasureSpec.EXACTLY)
            }
        }
        setMeasuredDimension(widthMeasureSpec, heightSize)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        progressY = h / 2 + indicatorMargin / 2 + heightText / 2
        leftThumbX = thumbRadius
        rightThumbX = w - thumbRadius
        recalculateThumbStep()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        onDrawProgress(canvas)
        onDrawLeftThumb(canvas)
        onDrawRightThumb(canvas)
        onDrawLeftIndicator(canvas)
        onDrawRightIndicator(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_UP -> {
                callback?.onRangeChanged(valueFrom, valueTo)

                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(false)
                }
            }
            MotionEvent.ACTION_DOWN -> {
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true)
                }
                val deltaLeft = abs(event.x - leftThumbX)
                val deltaRight = abs(event.x - rightThumbX)

                activeThumb = if (deltaLeft < deltaRight) Thumb.LEFT else Thumb.RIGHT

                if (activeThumb == Thumb.LEFT) {
                    valueFrom = setIndicatorValue(event.x)
                } else {
                    valueTo = setIndicatorValue(event.x)
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if (activeThumb == Thumb.LEFT) {
                    valueFrom = setIndicatorValue(event.x)
                } else {
                    valueTo = setIndicatorValue(event.x)
                }

                invalidate()
            }
        }

        return true
    }

    private fun setIndicatorValue(x: Float) =
        when {
            activeThumb == Thumb.LEFT && x < thumbRadius -> rangeFrom
            activeThumb == Thumb.RIGHT && x > width - thumbRadius -> rangeTo
            activeThumb == Thumb.LEFT && x > rightThumbX - 2 * thumbRadius ->
                calculateIndicatorValue(rightThumbX - 2 * thumbRadius)
            activeThumb == Thumb.RIGHT && x < leftThumbX + 2 * thumbRadius ->
                calculateIndicatorValue(leftThumbX + 2 * thumbRadius)
            else -> calculateIndicatorValue(x)
        }

    private fun calculateIndicatorValue(x: Float) =
        ((x - thumbRadius) / thumbStep).roundToInt() + rangeFrom

    @SuppressLint("ResourceAsColor")
    private fun onDrawProgress(canvas: Canvas?) {
        paintSeekBar.color = seekBarColorRes

        rectF.set(
            thumbRadius,
            progressY + progressRadius,
            width.toFloat() - thumbRadius,
            progressY - progressRadius
        )

        canvas?.drawRoundRect(rectF, progressCornerRadius, progressCornerRadius, paintSeekBar)
    }

    private fun calculateThumbX(indicatorValue: Int) =
        thumbStep * (indicatorValue + thumbRadius / thumbStep - rangeFrom)

    @SuppressLint("ResourceAsColor")
    private fun onDrawLeftThumb(canvas: Canvas?) {
        val thumbX = calculateThumbX(valueFrom)
        leftThumbX =
            when {
                valueFrom == rangeFrom -> thumbRadius
                thumbX < thumbRadius -> thumbRadius
                else -> thumbX
            }

        onDrawInactiveLeftRange(canvas)

        paintSeekBar.color = seekBarColorRes

        canvas?.drawCircle(leftThumbX, progressY, thumbRadius, paintSeekBar)
    }

    @SuppressLint("ResourceAsColor")
    private fun onDrawRightThumb(canvas: Canvas?) {
        val thumbX = calculateThumbX(valueTo)
        rightThumbX =
            when {
                valueTo == rangeTo -> width - thumbRadius
                thumbX > width - thumbRadius -> width - thumbRadius
                else -> thumbX
            }

        onDrawInactiveRightRange(canvas)

        paintSeekBar.color = seekBarColorRes
        canvas?.drawCircle(rightThumbX, progressY, thumbRadius, paintSeekBar)
    }

    @SuppressLint("ResourceAsColor")
    private fun onDrawLeftIndicator(canvas: Canvas?) {
        val text = "$valueFrom $valueUnit"

        paintText.color = textColorRes
        leftIndicatorWidth = paintText.measureText(text)
        rightIndicatorWidth = paintText.measureText("$valueTo $valueUnit")

        leftTextX = leftThumbX - (leftIndicatorWidth / 2)
        rightTextX = rightThumbX - (rightIndicatorWidth / 2)

        val textX =
            when  {
                leftTextX < 0F || ((leftThumbX + rightThumbX) / 2) - leftIndicatorWidth < 0F -> 0F
                leftTextX + leftIndicatorWidth > width - leftIndicatorWidth
                        && ((leftThumbX + rightThumbX) / 2) + rightIndicatorWidth > width -> width - rightIndicatorWidth - leftIndicatorWidth
                leftTextX + leftIndicatorWidth > rightTextX -> ((leftThumbX + rightThumbX) / 2) - leftIndicatorWidth
                else -> leftTextX
            }
        textY = progressY - thumbRadius - indicatorMargin
        canvas?.drawText(text, textX, textY, paintText)
    }

    private fun onDrawRightIndicator(canvas: Canvas?) {
        val text = "$valueTo $valueUnit"
        val textX =
            when {
                rightTextX + rightIndicatorWidth > width || ((leftThumbX + rightThumbX) / 2) + rightIndicatorWidth > width -> width - rightIndicatorWidth
                rightTextX < 0F + leftIndicatorWidth && ((leftThumbX + rightThumbX) / 2) - leftIndicatorWidth < 0F -> 0F + leftIndicatorWidth
                leftTextX + leftIndicatorWidth > rightTextX -> (leftThumbX + rightThumbX) / 2
                else -> rightTextX
            }
        canvas?.drawText(text, textX, textY, paintText)
    }

    @SuppressLint("ResourceAsColor")
    private fun onDrawInactiveLeftRange(canvas: Canvas?) {
        if (leftThumbX <= 2 * thumbRadius) return

        paintSeekBar.color = inactiveSeekBarColorRes

        rectF.set(
            thumbRadius,
            progressY + progressRadius,
            leftThumbX,
            progressY - progressRadius
        )

        canvas?.drawRoundRect(rectF, progressCornerRadius, progressCornerRadius, paintSeekBar)
    }

    @SuppressLint("ResourceAsColor")
    private fun onDrawInactiveRightRange(canvas: Canvas?) {
        if (rightThumbX >= width - 2 * thumbRadius) return

        paintSeekBar.color = inactiveSeekBarColorRes

        rectF.set(
            rightThumbX,
            progressY + progressRadius,
            width - thumbRadius,
            progressY - progressRadius
        )

        canvas?.drawRoundRect(rectF, progressCornerRadius, progressCornerRadius, paintSeekBar)

    }

    fun setOnRangeChanged(listener: OnRangeChangedListener) {
        callback = listener
    }

    interface OnRangeChangedListener {
        fun onRangeChanged(valueFrom: Int, valueTo: Int)
    }

    enum class Thumb { NONE, LEFT, RIGHT }

}